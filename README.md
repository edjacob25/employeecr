﻿# Employee CR 
A small project using Razor Pages to add and show Employees.

## Tech 
It uses ASPNet Core and Entity Framework Core as base technologies


## Running
It is a simple solution containing only one project. As such, it can be easily run by using the command
```bash
dotnet run
```

As this is a simple proof of concept, when running in the default configuration (`Development`), the database will be created and migration run at startup. After that you can use it easily by default at [http://localhost:5124](http://localhost:5124)