﻿using System.ComponentModel.DataAnnotations;

namespace EmployeeStore.Models;

public class Employee
{
    public int Id { get; set; }

    [Required] public string? Name { get; set; }

    [Required, RegularExpression("^[A-Za-zñÑ&]{3,4}\\d{6}\\w{3}$"),]
    public string? Rfc { get; set; }

    [Required] [DataType(DataType.Date)] public DateTime DateOfBirth { get; set; }

    public EmployeeStatus Status { get; set; }
}

public enum EmployeeStatus
{
    NotSet,
    Active,
    Inactive
}