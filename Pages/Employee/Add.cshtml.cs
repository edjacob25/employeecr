﻿using EmployeeStore.Common;
using EmployeeStore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace EmployeeStore.Pages.Employee;

public class AddPageModel : PageModel
{
    private readonly EmployeeDBContext _ctx;

    public AddPageModel(EmployeeDBContext ctx)
    {
        _ctx = ctx;
    }

    public void OnGet()
    {
    }

    [BindProperty] public Models.Employee? Employee { get; set; }

    public string? Error { get; set; }

    public async Task<IActionResult> OnPostAsync()
    {
        if (!ModelState.IsValid)
        {
            Error = "Model is invalid";
            return Page();
        }

        if (Employee != null) _ctx.Employees.Add(Employee);

        try
        {
            await _ctx.SaveChangesAsync();
        }
        catch (DbUpdateException e)
        {
            Error = "RFC already in use";
            return Page();
        }


        return RedirectToPage("./Show");
    }
}