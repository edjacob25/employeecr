﻿using EmployeeStore.Common;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace EmployeeStore.Pages.Employee;

public class ShowPageModel : PageModel
{
    private readonly EmployeeDBContext _ctx;

    public ShowPageModel(EmployeeDBContext ctx)
    {
        _ctx = ctx;
    }

    public IEnumerable<Models.Employee> Employees { get; set; }

    public async void OnGet(string? name)
    {
        var emps = string.IsNullOrEmpty(name) ? _ctx.Employees : _ctx.Employees.Where(e => e.Name.Contains(name));
        Employees = await emps
            .OrderBy(e => e.DateOfBirth)
            .ToListAsync();
        ViewData["query"] = name;
    }
}