﻿using EmployeeStore.Models;
using Microsoft.EntityFrameworkCore;

namespace EmployeeStore.Common;

public class EmployeeDBContext : DbContext
{
    public EmployeeDBContext(DbContextOptions<EmployeeDBContext> options)
        : base(options)
    {
    }

    public DbSet<Employee> Employees { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Employee>().HasIndex(e => e.Rfc).IsUnique();
        base.OnModelCreating(modelBuilder);
    }
}